

#define CANDLE_1 2
#define CANDLE_2 3
#define CANDLE_3 4
#define CANDLE_4 5

#define HELPER_CANDLE 6

#define DELAY 5

#define CANDLE_5 7
#define CANDLE_6 8
#define CANDLE_7 9
#define CANDLE_8 10

#define BUTTON_COOLDOWN 500
#define INPUT_PIN 12
int night;
int timeout;

void setup() {
  pinMode(INPUT_PIN, INPUT_PULLUP);

  pinMode(CANDLE_1, OUTPUT);
  pinMode(CANDLE_2, OUTPUT);
  pinMode(CANDLE_3, OUTPUT);
  pinMode(CANDLE_4, OUTPUT);

  pinMode(HELPER_CANDLE, OUTPUT);

  pinMode(CANDLE_5, OUTPUT);
  pinMode(CANDLE_6, OUTPUT);
  pinMode(CANDLE_7, OUTPUT);
  pinMode(CANDLE_8, OUTPUT);
  night = 0;
  timeout = 0;
}

// the loop function runs over and over again forever
void loop() {
  
  digitalWrite(HELPER_CANDLE, HIGH);   
  digitalWrite(HELPER_CANDLE, HIGH);   

  if(night > 0)
  digitalWrite(CANDLE_1, HIGH);   
  if(night > 1)
  digitalWrite(CANDLE_2, HIGH);   
  if(night > 2)
  digitalWrite(CANDLE_3, HIGH);   
  if(night > 3)
  digitalWrite(CANDLE_4, HIGH);   
  if(night > 4)
  digitalWrite(CANDLE_5, HIGH);   
  if(night > 5)
  digitalWrite(CANDLE_6, HIGH);   
  if(night > 6)
  digitalWrite(CANDLE_7, HIGH);   
  if(night > 7)
  digitalWrite(CANDLE_8, HIGH);   

  if(timeout>0){
    timeout -= DELAY*2;
    Serial.println("timeout");
    Serial.println(timeout);    
  } else {
    if(digitalRead(INPUT_PIN) == LOW){
      night+=1;
      timeout+=BUTTON_COOLDOWN;
      if(night > 8){
        night = 0;
      }
    }
  }
  
  delay(DELAY);                       
  digitalWrite(HELPER_CANDLE, LOW);   
  digitalWrite(CANDLE_1, LOW);  
  digitalWrite(CANDLE_2, LOW);  
  digitalWrite(CANDLE_3, LOW);  
  digitalWrite(CANDLE_4, LOW);  
  digitalWrite(CANDLE_5, LOW);  
  digitalWrite(CANDLE_6, LOW);  
  digitalWrite(CANDLE_7, LOW);  
  digitalWrite(CANDLE_8, LOW);   
  delay(DELAY);                       
}
